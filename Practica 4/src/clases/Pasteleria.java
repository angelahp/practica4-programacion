package clases;

import java.time.LocalDate;
import java.util.Scanner;

/**
 * 
 * @author Angela Hernandez
 *
 */

public class Pasteleria {
	
	private Pastelito[] pasteles;
	Scanner scan = new Scanner(System.in);
	
	public Pasteleria(int size) {
		this.pasteles = new Pastelito[size];		
	}
	
	/** 
	 * Da de alta un nuevo Pastelito
	 * 
	 * @param nombre del Pastelito
	 * @param sabor del Pastelito
	 * @param textura del Pastelito
	 * @param fecha del Pastelito
	 * @param peso del Pastelito
	 * @param precio del Pastelito
	 * @param calorias del Pastelito
	 * 
	 * */
	
	public void altaPastelito(String nombre, String sabor, String textura ,
			LocalDate fecha, double peso, double precio,	double calorias) {
		
		for (int i = 0; i < pasteles.length; i++) {
			if(pasteles[i] == null) {
				pasteles[i] = new Pastelito(nombre);
				pasteles[i].setSabor(sabor);
				pasteles[i].setTextura(textura);
				pasteles[i].setPeso(peso);
				pasteles[i].setPrecio(precio);
				pasteles[i].setCalorias(calorias);
				pasteles[i].setFecha(fecha);
				
				break;
			}
		}
	}
	
	/**
	 * Buscar un Pastelito por su nombre
	 * @param nombre que se va a buscar
	 * @return objeto Pastelito
	 *  */
	
	public Pastelito buscarPastelito(String nombre) {
		
		for (int i = 0; i < pasteles.length; i++) {
			if(pasteles[i] != null) {
				if(pasteles[i].getNombre().equals(nombre)) {
					return pasteles[i];
				}
			}
		}
		return null;
	}
	
	/**
	 * Elimina los Pastelitos con el nombre introducido
	 * @param nombre que se va a eliminar
	 */
	
	public void eliminarPastelito(String nombre) {
		
		for (int i = 0; i < pasteles.length; i++) {
			if(pasteles[i] != null) {
				if(pasteles[i].getNombre().equals(nombre)) {
					pasteles[i] = null;
				}
			} 
		}
		
	}
	
	/** 
	 * Muestra por pantalla todos los Pastelitos creados 
	 */
	
	public void listarPastelitos() {
		
		for (int i = 0; i < pasteles.length; i++) {
			if(pasteles[i] != null) {
				System.out.println(pasteles[i]);
			} 
		}	
	}
	
	/** 
	 * Muestra por pantalla todos los Pastelitos que tengan el Sabor introducido
	 * @param sabor por el que se va a hacer la busqueda
	 */
	
	public void listarPorSabor(String sabor) {
		
		for (int i = 0; i < pasteles.length; i++) {
			if(pasteles[i] != null) {
				if(pasteles[i].getSabor().equals(sabor)) {
					System.out.println(pasteles[i].toString());
				}
			}
		}
	}
	
	/** 
	 * Cambia el nombre de un Pastelito determinado
	 * @param nombre original (el que se quiere cambiar)
	 * @param nombre2 nuevo (el que sustituye al anterior)
	 */
	
	public void cambiarNombrePastelito(String nombre, String nombre2) {
		
		for (int i = 0; i < pasteles.length; i++) {
			if(pasteles[i] != null) {
				if(pasteles[i].getNombre().equals(nombre)) {
					pasteles[i].setNombre(nombre2);
				} else {
					System.out.println("El nombre introducido no se corresponde con ningun pastelito");
				}
			}
		}
	}
	

	/** 
	 * Cambia el sabor de un Pastelito determinado
	 * @param sabor original (el que se quiere cambiar)
	 * @param sabor2 nuevo (el que sustituye al anterior)
	 */
	
	public void cambiarSaborPastelito(String sabor, String sabor2) {
		
		for (int i = 0; i < pasteles.length; i++) {
			if(pasteles[i] != null) {
				if(pasteles[i].getSabor().equals(sabor)) {
					pasteles[i].setSabor(sabor2);
				}
			}
		}
	}
	
	/** 
	 * Muestra los datos de un Pastelito determinado
	 * @param nombre del Pastelito que se quiere mostrar
	 */

	public void mostrarUnPastelito(String nombre) {
		
		for (int i = 0; i < pasteles.length; i++) {
			if(pasteles[i] != null) {
				if(pasteles[i].getNombre().equals(nombre)) {
					
					System.out.println(pasteles[i].toString());
				}
			}
		}	
	}
	
	/* METODOS EXTRAS */
	
	/** 
	 * Metodo que muestra el Pastelito con m�s calorias
	 * 
	 */
	
	public void mostrarMaxCalorias() {
		
		double maxCalorias = pasteles[0].getCalorias();
		String nombreMaxCalorias = pasteles[0].getNombre();
		
		for (int i = 0; i < pasteles.length; i++) {
			if(pasteles[i] != null) {
				if(pasteles[i].getCalorias() > maxCalorias) {
					maxCalorias = pasteles[i].getCalorias();
					nombreMaxCalorias = pasteles[0].getNombre();
				}
			}
		}	
		
		System.out.println("El pastelito con + cal es "+ nombreMaxCalorias + " y tiene " + maxCalorias + " calorias");
	}
	
	/** 
	 * Metodo que muestra el Pastelito que antes caduca o el que mas tarda en caducar (se puede elegir)
	 * 
	 */
	
	public void mostrarPastelitoCaducidad() {
		
		int opcion;
		
		do {
			System.out.println("1. Mostrar ultimo Pastelito en caducar");
			System.out.println("2. Mostrar primer Pastelito en caducar");
			
			System.out.println("Introduce una opcion (-1 para Salir): ");
			opcion = scan.nextInt();
			
			switch(opcion) {
			
			case -1:
				System.exit(0);
				break;
			
			case 1: {
				scan.nextLine();
				System.out.println("\n\n * Pastelito que caduca mas tarde *\n");
				
				
				LocalDate fechaMax = pasteles[0].getFecha();
				String nombreMax = "";
					
				for (int i = 0; i < pasteles.length; i++) {
					if(pasteles[i] != null) {
							LocalDate fechaPastelito = pasteles[i].getFecha();
							if(fechaPastelito.isAfter(fechaMax)) {
								fechaMax = fechaPastelito;
								nombreMax = pasteles[i].getNombre();
							}
					}
				}
				
				System.out.println("El Pastelito " + nombreMax + " es el que caduca mas tarde.");
				System.out.println("Caduca el: " + fechaMax + "\n\n" );
				
				break;
				}//case 1
			
			case 2: {
				scan.nextLine();
				System.out.println("\n\n * Pastelito que primero caduca *\n");
				
				LocalDate fechaMin = pasteles[0].getFecha();
				String nombreMin = "";
					
				for (int i = 0; i < pasteles.length; i++) {
					if(pasteles[i] != null) {
						LocalDate fechaPastelito = pasteles[i].getFecha();
						if(fechaPastelito.isBefore(fechaMin)) {
							fechaMin = fechaPastelito;
							nombreMin = pasteles[i].getNombre();	
						}
					}
				}
				
				System.out.println("El Pastelito " + nombreMin + " es que antes caduca .");
				System.out.println("Caduca el: " + fechaMin + "\n\n" );
				break;
				}//case 2
			}//switch
		} while(opcion != -1);
				
	}
		
	/** 
	 * Metodo que asigna valor a la variable caducado 
	 * @param nombre del Pastelito
	 * @return caducado (tendra valor true si esta caducado y false si no lo esta)
	 */
	
	public boolean caducidad(String nombre) {
		LocalDate fechaActual = LocalDate.now();
		boolean caducado = false;
		
		for (int i = 0; i < pasteles.length; i++) {
			if(pasteles[i] != null) {
				if(pasteles[i].getNombre().equals(nombre)) {
					LocalDate fechaPastelito = pasteles[i].getFecha();
					if(fechaPastelito.isAfter(fechaActual)) {
						caducado = false;
					} else {						
						caducado = true;
					}
				}
			}
		}
		return caducado;
	}
	
	/** 
	 * Metodo que muestra por pantalla el resultado del metodo anterior
	 * @param nombre del Pastelito
	 */
	
	public void comprobarCaducidad(String nombre) {
		
		System.out.println("nombre: ");
		nombre = scan.nextLine();
		
		if(caducidad(nombre)) {
			System.out.println("El Pastelito ha caducado :(");
		} else {
			System.out.println("El Pastelito no ha caducado :)");
		}
		
	}
	
}