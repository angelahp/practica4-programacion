package clases;

import java.time.LocalDate;
/**
 * Clase en la que se guarda la informacion de distintos Pastelitos
 * @author Angela Hernandez
 *
 */

public class Pastelito {
	
	// Atributos: 
	private String nombre;
	private String sabor;
	private String textura;
	private double peso;
	private double precio;
	private double calorias;
	private LocalDate fecha;
	
	 
	/**
	 * Constructor
	 * genera un nuevo Pastelito dado un nombre
	 * @param nombre es el identificador que tendra el Pastelito
	 */
	public Pastelito(String nombre) {
		
		this.nombre = nombre;
	}
	// Setters y Getters
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getSabor() {
		return sabor;
	}
	public void setSabor(String sabor) {
		this.sabor = sabor;
	}
	public String getTextura() {
		return textura;
	}
	public void setTextura(String textura) {
		this.textura = textura;
	}
	public double getPeso() {
		return peso;
	}
	public void setPeso(double peso) {
		this.peso = peso;
	}
	public double getPrecio() {
		return precio;
	}
	public void setPrecio(double precio) {
		this.precio = precio;
	}
	public double getCalorias() {
		return calorias;
	}
	public void setCalorias(double calorias) {
		this.calorias = calorias;
	}
	
	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}
	
	//toString
	@Override
	public String toString() {
		
		return "\nNombre: " + getNombre() + "\nSabor: " + getSabor() + "\nTextura: " 
				+ getTextura() + "\nFecha: " + getFecha()	+ "\nPeso: " + getPeso() 
				+ "\nPrecio: " + getPrecio() + "\nCalorias: " + getCalorias() + "\n";
	}

}
