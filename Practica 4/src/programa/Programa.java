package programa;

import java.time.LocalDate;
import java.util.Scanner;

import clases.Pasteleria;

public class Programa {
	
	static Scanner scan = new Scanner(System.in);
	static Pasteleria miPasteleria;
	
	public static void main(String[] args) {
		bienvenida();
		
		System.out.println("Introduce el maximo de Pastelitos que quieres crear: ");
		int cantidad = scan.nextInt();
		miPasteleria = new Pasteleria(cantidad);
		int opcion = 0;
		
		do {
			mostrarOpcion();
			opcion = scan.nextInt();
			ejecutarOpcion(opcion);

		} while (opcion != -1 );
	
	}
	
	/** 
	 * Este metodo muestra por pantalla el menu de opciones
	 */
	
	public static void bienvenida() {
		
		System.out.println("     _                       _             \r\n" + 
				"    | |                     (_)            \r\n" + 
				" ___| |__   ___  _ __  _ __  _ _ __   __ _ \r\n" + 
				"/ __| '_ \\ / _ \\| '_ \\| '_ \\| | '_ \\ / _` |\r\n" + 
				"\\__ \\ | | | (_) | |_) | |_) | | | | | (_| |\r\n" + 
				"|___/_| |_|\\___/| .__/| .__/|_|_| |_|\\__, |\r\n" + 
				"                | |   | |             __/ |\r\n" + 
				"                |_|   |_|            |___/ ");
		
		System.out.println(	"\n\t╔═══════════════════════╗\r\n" + 
							"\t║ PASTELERIAS HERNANDEZ ║\r\n" + 
							"\t╚═══════════════════════╝");
		
		System.out.println("______________________________________________________________________\n");
		System.out.println("\tHola ♥! Bienvenid@ a la Pastelería de Angela");
		System.out.println("\tPor aqui te dejo las diferentes cositas que puedes hacer ");
		
		System.out.println("______________________________________________________________________");
	}
	
	public static void mostrarOpcion() {
				
		System.out.println("\n1. Añadir nuevo Pastelito.");
		System.out.println("2. Buscar Pastelito.");
		System.out.println("3. Eliminar Pastelito.");
		System.out.println("4. Listar todos los Pastelitos.");
		System.out.println("5. Cambiar Nombre Pastelito.");
		System.out.println("6. Cambiar Sabor Pastelito.");
		System.out.println("7. Muestra los datos de un Pastelito.");
		System.out.println("8. Listar por Sabor.");
		System.out.println("9. Muestra el Pastelito que tiene mas Calorias.");
		System.out.println("10. Comprobar si el Pastelito ha caducado.");
		System.out.println("11. Mostrar Pastelito que caduca primero o el ultimo en caducar.");
		System.out.println("- Pssh Pssh, si pones 12, solo hay que introducir las fechas" 
							+ " pero no se lo digas a nadie... ");
		
		System.out.println("Introduce una opcion (0 para salir): ");
			
	}
	
	/** 
	 * Este metodo ejecuta una de las opciones del metodo anterior (mostrarOpcion())
	 * @param opcion que se introduce por teclado
	 */
	
	public static void ejecutarOpcion(int opcion) {
		
		switch(opcion) {
		
		case 0:
			System.exit(0);
			break;
		
		case 1:/*Añadir*/
			System.out.println("\t-> Añadir Pastelitos <-");
			scan.nextLine();
			
			System.out.println("Introduce el nombre del nuevo Pastelito: ");
			String nombre = scan.nextLine();
			System.out.println("Introduce el sabor del nuevo Pastelito: ");
			String sabor = scan.nextLine();
			System.out.println("Introduce la textura del nuevo Pastelito: ");
			String textura = scan.nextLine();
			System.out.println("Introduce fecha de caducidad del nuevo Pastelito (yyyy-mm-dd): ");
			String fecha = scan.nextLine();
			LocalDate parseFecha = LocalDate.parse(fecha);
			
			System.out.println("Introduce el peso del nuevo Pastelito");
			double peso = scan.nextDouble();
			System.out.println("Introduce el precio del nuevo Pastelito");
			double precio = scan.nextDouble();
			System.out.println("Introduce las calorias del nuevo Pastelito");
			double calorias = scan.nextDouble();
			
			miPasteleria.altaPastelito(nombre, sabor, textura, parseFecha, peso, precio, calorias);
			break;
			
		case 2:/*Buscar*/
			System.out.println("\t-> Buscar Pastelito <-");
			scan.nextLine();
			
			System.out.println("Introduce el nombre del Pastelito que quieres buscar: ");
			nombre = scan.nextLine();
			
			miPasteleria.buscarPastelito(nombre);
			miPasteleria.mostrarUnPastelito(nombre);
			break;
		
		case 3:/*Eliminar*/
			System.out.println("\t-> Eliminar Pastelitos <-");
			scan.nextLine();
			
			System.out.println("Introduce el nombre del pastelito que quieres eliminar");
			nombre = scan.nextLine();
			
			miPasteleria.eliminarPastelito(nombre);
			miPasteleria.listarPastelitos();
			
			break;
		
		case 4:/*Listar*/
			System.out.println("\t-> Lista de todos los Pastelitos <-");
			miPasteleria.listarPastelitos();
			
			break;
		
		case 5:/*Cambiar Nombre Pastelito*/
			System.out.println("\t-> Cambiar Nombre del Pastelito <-");
			scan.nextLine();

			System.out.println("Introduce el nombre del Pastelito al que le quieres cambiar el nombre");
			nombre = scan.nextLine();
			
			System.out.println("Datos del pastelito que vas a modificar: ");
			miPasteleria.mostrarUnPastelito(nombre);
			
			System.out.println("Introduce el nuevo nombre del Pastelito: ");
			String nuevoNombre = scan.nextLine();
			
			miPasteleria.cambiarNombrePastelito(nombre, nuevoNombre);
			
			System.out.println("Datos del pastelito modificado: ");
			miPasteleria.mostrarUnPastelito(nuevoNombre);
			
			break;
		
		case 6:/*Cambiar Sabor Pastelito*/
			System.out.println("\t-> Cambiar Sabor del Pastelito <-");
			scan.nextLine();
			
			System.out.println("Introduce el sabor del Pastelito al que le quieres cambiar el sabor");
			sabor = scan.nextLine();
			
			System.out.println("Datos del pastelito que vas a modificar: ");
			miPasteleria.mostrarUnPastelito(sabor);
			
			System.out.println("Introduce el nuevo sabor del Pastelito: ");
			String nuevoSabor = scan.nextLine();
			
			miPasteleria.cambiarNombrePastelito(sabor, nuevoSabor);
			
			System.out.println("Datos del pastelito modificado: ");
			miPasteleria.mostrarUnPastelito(nuevoSabor);
						
			break;
		
		case 7:/* Mostrar datos de un Pastelito*/
			System.out.println("\t-> Mostrar datos de un Pastelito <-");
			scan.nextLine();
			
			System.out.println("Introduce el nombre Pastelito");
			nombre = scan.nextLine();
			
			miPasteleria.mostrarUnPastelito(nombre);
			
			break;
			
		case 8: /*Listar por Sabor Pastelito*/
			System.out.println("\t-> Lista de Pastelitos por Sabor <-");
			scan.nextLine();
			
			System.out.println("Introduce el sabor por el que quieres listar: ");
			sabor = scan.nextLine();
			
			miPasteleria.listarPorSabor(sabor);
			
			break;
			
		
		case 9:/*ordena de Menor a Mayor por Calorias*/	
			System.out.println("\t-> Muestra Pastelito que tiene mas calorias <-");
			miPasteleria.mostrarMaxCalorias();
					
			break;
			
		case 10:/* Mostrar si un Pastelito esta caducado o no */
			scan.nextLine();
			System.out.println("\t-> Mostrar si el Pastelito ha caducado o no<-");
			
			
			System.out.println("Introduce el nombre Pastelito");
			nombre = scan.nextLine();
			
			miPasteleria.comprobarCaducidad(nombre);

			break;
		
		case 11: /* Mostrar Pastelito que caduca primero o el ultimo en caducar */
			
			System.out.println("\t-> Mostrar Pastelito que caduca primero o el ultimo en caducar<-");
			miPasteleria.mostrarPastelitoCaducidad();
		
		case 12:
			/* Este caso sirve para no tener que rellenar todos los datos por teclado */
			scan.nextLine();
					
			System.out.println("Introduce fecha de caducidad del Pastelito1 (yyyy-mm-dd): ");
			String fecha1 = scan.nextLine();
			LocalDate parseFecha1 = LocalDate.parse(fecha1);
						
			System.out.println("Introduce fecha de caducidad del Pastelito2 (yyyy-mm-dd): ");
			String fecha2 = scan.nextLine();
			LocalDate parseFecha2 = LocalDate.parse(fecha2);
			
			System.out.println("Introduce fecha de caducidad del Pastelito3 (yyyy-mm-dd): ");
			String fecha3 = scan.nextLine();
			LocalDate parseFecha3 = LocalDate.parse(fecha3);
			
			System.out.println("Introduce fecha de caducidad del Pastelito4 (yyyy-mm-dd): ");
			String fecha4 = scan.nextLine();
			LocalDate parseFecha4 = LocalDate.parse(fecha4);
			
			miPasteleria.altaPastelito("Bizcocho", "chocolate", "esponjoso", parseFecha1, 500, 6.4, 2325);
			miPasteleria.altaPastelito("Galletas", "chocolate", "perfectas", parseFecha2, 2, 1.75, 300);
			miPasteleria.altaPastelito("Magdalenas", "normales", "esponjosas", parseFecha3, 5, 2.75, 250);
			miPasteleria.altaPastelito("Creepes", "chocolate", "suaves", parseFecha4, 30, 4.5, 124);
			break;
		
		default:
			System.out.println("Opcion introducida no es correcta");
			break;
		
		
		}
		
	}

}